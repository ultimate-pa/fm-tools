<!--
This file is part of lib-fm-tools, a library for interacting with FM-Tools files:
https://gitlab.com/sosy-lab/benchmarking/fm-tools

SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

## Libraries for interacting with FM-Tools YAML files

This folder contains libraries for interacting with FM-Tools YAML files.