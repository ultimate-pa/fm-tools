<!--
This file is part of FM-Tools, a data collection about formal-methods tools.
https://gitlab.com/sosy-lab/benchmarking/fm-tools

SPDX-FileCopyrightText: 2025 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: CC-BY-4.0
-->

This directory contains logos for tools described in the `data/` directory.

A logo entry consists of two entries:
- `logos/<tool-id>.svg` and
- `logos/<tool-id>.svg.license`
where `<tool-id>` matches one of the file names `data/<tool-id>.yml`.

A logo file should not be larger than 100 kB.

The file with extension `license` contains an SPDX identifier of the license.
Please use the following contents for this file:
```
SPDX-License-Identifier: CC-BY-4.0
```
Instead of CC-BY-4.0 you can use a different license,
but CC-BY-4.0 is strongly recommended for logo files.

Examples:
https://gitlab.com/sosy-lab/benchmarking/fm-tools/-/tree/main/logos

